# Street Art Paint Colour Palettes for GIMP 2

A repository of colour palettes of major brands of paint for street art, for use with open source graphic software [GIMP 2](https://www.gimp.org/). 

Useful if you are planning a mural but want to play around with combinations of colours digitally before committing to buying the spray cans or paint pens. 

*DISCLAIMER:* RGB colours behave differently to real physical paint so no guarantee of accuracy is offered. I simply picked these off the manufacturers own website. 

## Brands and Ranges 

- `MTN94.gpl` - MTN 94 range as of May 2020 (English names). All colours excluding spectral (semi-transparent) and metalics. 
- MTN Hardcore TODO: Please contribute
- Posca Pens TODO: Please contribute
- Molotow TODO: Please contribute

### How to use them

Click on the `.gpl` file that you want and from the 3-dot menu choose "Open Raw" to view the raw file in your browser. You can now download and save the `.gpl` file(s) into the palette folder on your own computer. To find out the location open GIMP and go to Edit > Preferences > Folders > Palettes. 

Alternatively, you can import a `.gpl` file by first opening the Palettes dialogue (Windows > Dockable Dialogues > Palettes), then while the Palettes dialogue tab is selected click the little arrow menu to the right called "Configure this tab" then Palettes Menu > Import Palette and then select your palette file to import. 

### Contribution guidelines ###

If you would like to contribute by adding a palette or updating an existing one feel free to create a pull request. If you are not sure how to do this, hit me up on Twitter [@martinjoiner](https://twitter.com/martinjoiner)
